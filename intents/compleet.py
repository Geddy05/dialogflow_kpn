# coding: utf-8

from string import Template
from constants import *
from response_objects import Button, Card


def ask_products(request, answer):
    """ Create buttons with the products for KPN Compleet """
    list_of_outages = [ ITV, INTERNET, LANDLINE,DIGITENNE]
    button_list = [Button(i, i) for i in list_of_outages]

    card = Card(answer, '', '', button_list)
    return request.create_button_response(card)


def compleet(request):
    """ Handle KPN COMPLEET request """

    products = request.parameters['products']
    products = [x.encode('UTF8') for x in products]

    print(products)

    num_contracts = request.parameters['count']
    answer = request.fulfillment

    """ Check for slot filling """
    if num_contracts == "" or not num_contracts:
        return request.create_text_response(answer)
    if len(products) == 0:
        return ask_products(request, answer)

    """ Check for discount / KPN Compleet """
    if num_contracts > 0:
        answer = check_discount(products,num_contracts)
    else:
        answer = ANSWER_NO_COMPLEET

    return request.create_text_response(answer)


def check_discount(products,num_contracts):

    """ Check selected products en select correct answer. """
    if ALL_IN_ONE in products:
        return ANSWER_ALL_IN_1
    elif len(products) > 1 or (ITV in products or INTERNET in products) :
        return ANSWER_MULTIPLE_PRODUCTS
            # .substitute(products=", ".join(products))
    elif LANDLINE in products:
        return LANDLINE_MULTIPLE_CONTRACTS
    elif DIGITENNE in products:
        if num_contracts > 1:
            return DIGITENNE_MULTIPLE_CONTRACTS
        else:
            return DIGITENNE_ONE_CONTRACT
    else:
        if num_contracts > 1:
            return ANSWER_MULTIPLE_CONTRACT
        else:
            return ANSWER_ONE_CONTRACT



