# coding: utf-8

from constants import *
from response_objects import Button, Card


def ask_platform(request):
    answer = request.fulfillment
    list_of_outages = [WINDOWS, MAC, MOBILE]
    button_list = [Button(i, i) for i in list_of_outages]

    card = Card(answer, '', '', button_list)
    return request.create_button_response(card)


def ask_mail(request):
    answer = request.fulfillment
    return request.create_text_response(answer)


def email_setup(request):
    """ Function for selecting the correrct answer matching with version and platform """

    # TODO: Select platform (WINDOWS, MAC or MOBIEL)
    email_programs = request.parameters['email_programs']
    platform = request.parameters['platform']
    version = request.parameters['version']
    answer = request.fulfillment[0]

    if platform == "":
        return ask_platform(request)
    if email_programs == "":
        return ask_mail(request)

    if (email_programs == "Outlook" or email_programs == "Apple Mail") and version >= 0:
        email_programs = email_programs + " " + str(int(version))

    #   Mail programs for windows
    if email_programs == OUTLOOK_2016 and WINDOWS in platform:
        answer += OUTLOOK_2016_WINDOWS
    elif email_programs == OUTLOOK_2010 and WINDOWS in platform:
        answer += OUTLOOK_2010_WINDOWS
    elif email_programs == VISTA_MAIL and WINDOWS in platform:
        answer += VISTA_MAIL_WINDOWS
    elif email_programs == MOZILLA and WINDOWS in platform:
        answer += MOZILLA_WINDOWS
    elif email_programs == WINDOWS_8_MAIL and WINDOWS in platform:
        answer += WINDOWS_8_MAIL_WINDOWS
    elif email_programs == WINDOWS_10_MAIL and WINDOWS in platform:
        answer += WINDOWS_10_MAIL_WINDOWS
    elif email_programs == OUTLOOK_2007 and WINDOWS in platform:
        answer += OUTLOOK_2007_WINDOWS
    elif email_programs == OUTLOOK_2013 and WINDOWS in platform:
        answer += OUTLOOK_2013_WINDOWS

    #   Mail programs for mac
    elif email_programs == OUTLOOK_2016 and MAC in platform:
        answer += OUTLOOK_2016_MAC
    elif email_programs == OUTLOOK_2011 and MAC in platform:
        answer += OUTLOOK_2011_MAC
    elif email_programs == APPLE_MAIL_10 and MAC in platform:
        answer += APPLE_MAIL_10_MAC
    elif email_programs == APPLE_MAIL_9 and MAC in platform:
        answer += APPLE_MAIL_9_MAC
    elif email_programs == APPLE_MAIL_8 and MAC in platform:
        answer += APPLE_MAIL_8_MAC
    elif email_programs == APPLE_MAIL_7 and MAC in platform:
        answer += APPLE_MAIL_7_MAC
    elif email_programs == APPLE_MAIL_5 and MAC in platform:
        answer += APPLE_MAIL_5_MAC

    else:
        # answer = str(email_programs)
        answer = ["Sorry deze combinatie van besturingssysteem en programma ken ik niet. ",
                  "Heb je vragen over e-mail van een andere provider zoals Gmail, Hotmail, of over een ander e-mail programma, stel dan je vraag op het  KPN Forum. Ga daarvoor naar het Forum van KPN: https://forum.kpn.com/?cid=dialog"]

    return request.create_text_response(answer)
