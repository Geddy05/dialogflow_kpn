# coding: utf-8

from constants import *
from response_objects import *


def ask_product(request, answer):
    list_of_outages = [All_SERVICE_HOME, INTERNET, ITV, DIGITENNE, LANDLINE, MOBILE]

    button_list = [Button(i, i) for i in list_of_outages]

    card = Card(answer, '', '', button_list)

    return request.create_button_response(card)


def ask_outage(request,product, answer):

    if product == ITV:
        list_of_outages = [LOST_VISUAL, BAD_VISUAL, BAD_AUDIO, A_SYNC_VISUALS_AUDIO, BANGING_SOUND]
    elif product == DIGITENNE:
        list_of_outages = [LOST_VISUAL, BAD_VISUAL, UNAVAILABLE_CHANNELS, PROBLEMS_OTHER_CARDS, PROBLEMS_SMARTCARD]
    else:
        return request.create_text_response(OUTAGE_FALLBACK_TEXT)

    button_list = [Button(i, i) for i in list_of_outages]

    card = Card(answer, '', '', button_list)
    return request.create_button_response(card)


def main_outage(request):
    product = request.parameters['products']
    outage = request.parameters['outages']
    answer = request.fulfillment

    if product == "" or not product:
        return ask_product(request, answer)
    if (outage == "" or not outage) and (product == ITV or product == DIGITENNE):
        return ask_outage(request, product, answer)

    #  Outage with ITV
    if product == ITV:
        if outage == LOST_VISUAL:
            answer[0] += ITV_LOST_VISUAL
        elif outage == BAD_VISUAL:
            answer[0] += ITV_BAD_VISUAL
        elif outage == BAD_AUDIO:
            answer[0] += ITV_BAD_AUDIO
        elif outage == A_SYNC_VISUALS_AUDIO:
            answer[0] += ITV_A_SYNC_VISUALS_AUDIO
        elif outage == BANGING_SOUND:
            answer[0] += ITV_BANGING_SOUND

    # Outage with DIGITENNE
    elif product == DIGITENNE:
        if outage == BAD_VISUAL or outage == LOST_VISUAL:
            answer[0] += DIGITENNE_LOST_VISUAL
        elif product == DIGITENNE and outage == UNAVAILABLE_CHANNELS:
            answer[0] += DIGITENNE_UNAVAILABLE_CHANNELS
        elif product == DIGITENNE and outage == PROBLEMS_OTHER_CARDS:
            answer[0] += DIGITENNE_UNAVAILABLE_CHANNELS
        elif product == DIGITENNE and outage == PROBLEMS_SMARTCARD:
            answer[0] += DIGITENNE_PROBLEMS_SMARTCARD

    elif product == All_SERVICE_HOME:
        answer = OUTAGE_ALL
    elif product == INTERNET:
        answer = OUTAGE_INTERNET
    elif product == LANDLINE:
        answer = OUTAGE_LANDLINE
    elif product == MOBILE:
        answer = OUTAGE_MOBILE
    return request.create_text_response(answer)


def error_codes(request):
    error_code = request.parameters['errorcodes']
    answer = request.fulfillment

    if error_code == "" or not error_code:
        return request.create_text_response(answer)

    if "foutcode" in error_code:
        error_code = int(error_code.split(' ')[1])
    else:
        error_code = int(error_code)

    if error_code in TV_ERROR:
        answer[0] += ANSWER_ERRORCODE_12
    elif error_code in ITV_ERROR:
        answer[0] += ANSWER_ERRORCODE_300
    elif error_code in CODE800:
        answer[0] += ANSWER_ERRORCODE_800
    elif error_code in CODE11:
        answer[0] += ANSWER_ERRORCODE_11
    elif error_code in CODE561:
        answer[0] += ANSWER_ERRORCODE_561
    else:
        answer

    return request.create_text_response(answer)




