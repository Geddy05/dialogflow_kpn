import json
from flask import make_response


def obj_dict(obj):
    """ By using this function in a JSON.dumps all the objects in a array are parsed to correct JSON"""
    return obj.__dict__


class Request:
    fulfillment = ""

    def __init__(self, request):
        jn = json.loads(request)
        result = jn['queryResult']
        self.action = result['action']
        self.intent = result['intent']['displayName']
        self.parameters = result['parameters']
        self.resolvedQuery = result['queryText']
        self.session = jn['session']
        self.message = result['queryText']

        if 'outputContexts' in result:
            self.outputContexts = result['outputContexts']

        if 'fulfillmentText' in result:
            """ Split the prompt form web interface in multiple Strings / Messages on --> """
            self.fulfillment = [x.strip() for x in result['fulfillmentText'].split('-->')]

    @staticmethod
    def create_response(resp):
        res = json.dumps(resp, default=obj_dict)
        r = make_response(res)
        r.headers['Content-Type'] = 'application/json'

        return r

    def create_text_response(self, text):
        if isinstance(text, basestring):
            text = [text]

        response = {"fulfillmentMessages": [
            {
                "text": {
                    "text": text
                }
            }
        ]}

        return self.create_response(response)

    def create_button_response(self, card):
        response = {"fulfillmentMessages": [
            {
                'card': card.__dict__
            }
        ]}

        return self.create_response(response)

    def create_suggestion_response(self, suggestions):
        response = {"fulfillmentMessages": [
            {
                'suggestions': suggestions.__dict__
            }
        ]}

        print(response)
        return self.create_response(response)
