from constants import *
from chatbase import Message


def chatbase_incoming(response):
    msg = Message(api_key=CHATBASE_API_KEY,
                  platform=CHATBASE_PLATFORM,
                  version=CHATBASE_BOT_VERSION,
                  user_id=response.session,
                  message=response.message,
                  intent=response.intent)
    msg.set_as_handled()
    msg.send()