## Dialogflow webhook in python on Google App Engine

A Dialogflow webhook on Google App Engine with the
[Flask micro framework](http://flask.pocoo.org).

## Run Locally
1. Install the [App Engine Python SDK](https://developers.google.com/appengine/downloads).
    You'll need python 2.7 and [pip 1.4 or later](http://www.pip-installer.org/en/latest/installing.html) installed too.

2. Clone this repo with

3. Install dependencies in the project's lib directory.
   Note: App Engine can only import libraries from inside your project directory. 
   execute from ROOT folder of the project.

   ```
   pip install -r requirements.txt -t lib
   ```
   
4. Run this project locally from the command line:

   ```
   dev_appserver.py .
   ```

5.  Visit the application [http://localhost:8080](http://localhost:8080)
    
6.  You can use [ngrok](https://ngrok.com/) for creating a tunnel between Dialogflow and your localhost for debugging.
    NOTE: It's highly recommended to use username and password
 
        ngrok http -auth="username:password" 8080
    
    See [the development server documentation](https://developers.google.com/appengine/docs/python/tools/devserver)
for options when running dev_appserver.


## Deploy
To deploy the application:

1. Use the [Admin Console](https://appengine.google.com) to create a
   project/app id. (App id and project id are identical)
1. [Deploy the
   application](https://cloud.google.com/appengine/docs/standard/python/getting-started/deploying-the-application) with

   ```
   gcloud app deploy app.yaml
   ```
1. Your application is now live at <your-app-id>.appspot.com


### Installing Libraries
See the [Third party
libraries](https://developers.google.com/appengine/docs/python/tools/libraries27)
page for libraries that are already included in the SDK.  To include SDK
libraries, add them in your app.yaml file. Other than libraries included in
the SDK, only pure python libraries may be added to an App Engine project.
