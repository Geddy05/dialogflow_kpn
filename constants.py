# coding: utf-8
from string import Template

""" This file contains all the response messages and all the entities from Dialogflow. """

# CHATBASE_API_KEY = "497da8e3-041c-481c-ab62-5a35a9970edf"
CHATBASE_API_KEY = "afc66348-16cf-4905-9ad1-6d030a6941c6"
CHATBASE_PLATFORM = "KPN_Mail_Demo"
CHATBASE_BOT_VERSION = "2.0"

#       PRODUCTS
ALL_IN_ONE = "Alles-in-1"
ITV = "Interactieve Televisie"
INTERNET = "Internet"
LANDLINE = "Vast Bellen"
DIGITENNE = "Digitenne"

#      /////
TELEVISIE = "Televisie"
All_SERVICE_HOME = "Alle diensten thuis"
MOBILE = "Mobiel"

#       ANSWERS KPN-COMPLEET
ANSWER_MULTIPLE_CONTRACT = ["Je hebt ook nu al recht op onderstaande voordelen. Meld je direct aan! "
                            "Als je al 2 of meer KPN Mobiel abonnementen hebt, "
                            "dan kom je direct in aanmerking voor KPN Compleet.Je hoeft je alleen nog maar aan te melden."
                            " Binnen 5 werkdagen worden je voordelen geactiveerd",
                            "Je hebt nu al recht op de volgende voordelen: €5, korting, gratis onderling bellen en onderling MB's delen.",
                            "Klik op de volgende link om je aan te melden: https://campagnes.kpn.com/krijg-meer/kpn-compleet/prive?icid=p=kpn-compleet:krijg-meer:cm:c=compleet."]
ANSWER_ONE_CONTRACT =["Op dit moment heb je één mobiel abonnement van KPN. Combineer je Mobiel met Internet en TV, dan ontvang je onderstaande voordelen. De voordelen ontvang je zolang je klant blijft en het kost je uiteraard niets extra. ",
                     "Je hebt nu al recht op de volgende voordelen: €5,- extra korting, gratis extra zenderpakket, " \
                     "dubbel zoveel data in de hele eu en gratis onderling bellen. ",
                     "Bekijk via de volgende link de Internet en TV pakketten: " \
                     "https://bestellen.kpn.com/?compleet=1&icid=p=kpn-compleet:krijg-meer:cm:c=compleet"]
ANSWER_ALL_IN_1 = ["Wat fijn dat je zowel Interactieve Televisie, internet en vast bellen hebt. " \
                    "Als je Internet en TV en Mobiel al combineert, dan kom je direct in aanmerking voor KPN Compleet." \
                    "Je hoeft je alleen nog maar aan te melden. Binnen 5 werkdagen worden je voordelen geactiveerd. ","Wil je nog meer voordeel? Aanmelden voor KPN compleet kan via deze link: " \
                    "https://campagnes.kpn.com/krijg-meer/kpn-compleet/prive?icid=p=kpn-compleet:krijg-meer:cm:c=compleet"]
# TODO: needs multiple messages
ANSWER_MULTIPLE_PRODUCTS = ["Goed nieuws! Je komt direct in aanmerking voor KPN compleet. Je hoeft je alleen nog maar aan te melden. Binnen 5 werkdagen worden je voordelen geactiveerd. ",
                            "Je hebt recht op de volgende voordelen: €5,- extra korting, "
                            "extra zenderpakket en dubbel zoveel data in de hele EU. ",
                            "Zie de volgende link om je aan te melden: "
                            "https://campagnes.kpn.com/krijg-meer/kpn-compleet/prive?icid=p=kpn-compleet:krijg-meer:cm:c=compleet"]
ANSWER_NO_COMPLEET = "Je komt nog niet in aanmerking voor KPN Compleet. " \
                     "Check onze tips om ook te profiteren van de voordelen van KPN Compleet Bekijk je mogelijke " \
                     "voordelen: https://www.kpn.com/compleet/nog-geen-kpn-klant.htm"
LANDLINE_MULTIPLE_CONTRACTS = ["Op dit moment heb je één of meer mobiele abonnementen en Vast Bellen van KPN. " \
                                 "Breid je Vast Bellen uit naar Internet en TV van KPN met Interactieve TV " \
                                 "en krijg extra voordeel.", "Combineer je Mobiel met Internet en Interactieve Televisie," \
                                 " dan ontvang je onderstaande voordelen. De voordelen ontvang je zolang je klant blijft " \
                                 "en het kost je uiteraard niets extra. €5,- extra korting, gratis extra zenderpakket," \
                                 " dubbel zoveel data in de hele eu en gratis onderling bellen. ","Bekijk via de volgende link de Internet en TV pakketten:" \
                                 " https://bestellen.kpn.com/?compleet=1&icid=p=kpn-compleet:krijg-meer:cm:c=compleet"]
DIGITENNE_MULTIPLE_CONTRACTS = ["Als je upgrade naar Interactieve Televisie krijgt je de volgende vooordelen:" \
                               " €5,- extra korting, gratis extra zenderpakket, " \
                               "dubbel zoveel data in de hele EU en gratis onderling bellen.","Bekijk via de volgende link de Internet en TV pakketten: " \
                               "https://bestellen.kpn.com/?compleet=1&icid=p=kpn-compleet:krijg-meer:cm:c=compleet"]
ONE_MOBILE_CONTRACT_DISCOUNT = ["Op dit moment heb je één mobiel abonnement van KPN. Combineer je Mobiel met Internet en TV, dan ontvang je onderstaande voordelen." \
                               " De voordelen ontvang je zolang je klant blijft en het kost je uiteraard niets ",
                                "Je hebt nu al recht op de volgende voordelen: extra. €5,- extra korting, gratis extra zenderpakket, dubbel zoveel data in de hele eu en gratis onderling bellen. ",
                                "Bekijk via de volgende link de Internet en TV pakketten: https://bestellen.kpn.com/?compleet=1&icid=p=kpn-compleet:krijg-meer:cm:c=compleet"]
DIGITENNE_ONE_CONTRACT = ["Helaas kan je Digitenne niet combineren met KPN Compleet. " \
                          "Op dit moment heb je één mobiel abonnement van KPN. " \
                          "Combineer je Mobiel met Internet en TV, dan ontvang je onderstaande voordelen. " \
                          "De voordelen ontvang je zolang je klant blijft en het kost je uiteraard niets extra.",
                          "Voordelen zijn: €5,- extra korting, gratis extra zenderpakket, " \
                          "dubbel zoveel data in de hele eu en gratis onderling bellen. ",
                          "Bekijk via de volgende link de Internet en TV pakketten: " \
                          "https://bestellen.kpn.com/?compleet=1&icid=p=kpn-compleet:krijg-meer:cm:c=compleet"]

#       PLATFORMS
WINDOWS = "Windows"
MAC = "Mac"

APPLE_MAIL_10 = "Apple Mail 10"
APPLE_MAIL_9 = "Apple Mail 9"
APPLE_MAIL_8 = "Apple Mail 8"
APPLE_MAIL_7 = "Apple Mail 7"
APPLE_MAIL_5 = "Apple Mail 5"
OUTLOOK_2016 = "Outlook 2016"
OUTLOOK_2013 = "Outlook 2013"
OUTLOOK_2011 = "Outlook 2011"
OUTLOOK_2010 = "Outlook 2010"
OUTLOOK_2007 = "Outlook 2007"
WINDOWS_10_MAIL = "Windows 10 Mail"
WINDOWS_8_MAIL = "Windows 8 Mail"
VISTA_MAIL = "Vista Mail"
MOZILLA = "Mozilla Thunderbird"

#       MAIL ANSWERS
OUTLOOK_2016_WINDOWS = " https://www.kpn.com/faq/18079?icid=dialog"
OUTLOOK_2013_WINDOWS = " https://www.kpn.com/faq/16404?icid=dialog"
OUTLOOK_2010_WINDOWS = " https://www.kpn.com/faq/14491?icid=dialog"
VISTA_MAIL_WINDOWS = " https://www.kpn.com/faq/9612?icid=dialog"
MOZILLA_WINDOWS = " https://www.kpn.com/faq/15018?icid=dialog"
WINDOWS_8_MAIL_WINDOWS = " https://www.kpn.com/faq/16053?icid=dialog"
WINDOWS_10_MAIL_WINDOWS = " https://www.kpn.com/faq/17486?icid=dialog"
OUTLOOK_2007_WINDOWS = " https://www.kpn.com/faq/7459?icid=dialog"
OUTLOOK_2016_MAC = " https://www.kpn.com/faq/18074?icid=dialog"
OUTLOOK_2011_MAC = " https://www.kpn.com/faq/14231?icid=dialog"
APPLE_MAIL_10_MAC = " https://www.kpn.com/faq/18905?icid=dialog"
APPLE_MAIL_9_MAC = " https://www.kpn.com/faq/18010?icid=dialog"
APPLE_MAIL_8_MAC = " https://www.kpn.com/faq/16993?icid=dialog"
APPLE_MAIL_7_MAC = " https://www.kpn.com/faq/16610?icid=dialog"
APPLE_MAIL_5_MAC = " https://www.kpn.com/faq/16017?icid=dialog"
MAIL_FALLBACK_MESSAGE = "Sorry deze combinatie ken ik niet. "

#       ERROR CODES
TV_ERROR = [12, 140, 141, 142, 143, 144, 843]
ITV_ERROR = [300, 301, 307, 311]
CODE800 = [840, 841, 845, 846]
CODE11 = [11, 842, 303]
CODE561 = [561, 14]

#        OUTAGE
LOST_VISUAL = "geen beeld"
BAD_VISUAL = "slecht beeld"
BAD_AUDIO = "slecht geluid"
A_SYNC_VISUALS_AUDIO = "asynchroon beeld en geluid"
BANGING_SOUND = "haperend geluid"
UNAVAILABLE_CHANNELS = "niet alle zenders beschikbaar"
PROBLEMS_OTHER_CARDS = "problemen met de insteekkaart"
PROBLEMS_SMARTCARD = "problemen met de smartcard"
OUTAGE_FALLBACK_TEXT= "Sorry deze combinatie van product en storing ken ik niet. " \
                      "Zie voor een volledige lijst met storingen: " \
                      "https://www.kpn.com/service/storingen.htm#/problemen-en-storingen"

#        OUTAGE_RESPONSES
ITV_LOST_VISUAL = " https://www.kpn.com/service/televisie/problemen/geenbeeld.htm#/"
ITV_BAD_VISUAL = " https://www.kpn.com/faq/17973"
ITV_BAD_AUDIO = " https://www.kpn.com/faq/17974"
ITV_A_SYNC_VISUALS_AUDIO = " https://www.kpn.com/faq/17979"
ITV_BANGING_SOUND = " https://www.kpn.com/faq/19150"
DIGITENNE_LOST_VISUAL = " https://www.kpn.com/faq/6204"
DIGITENNE_UNAVAILABLE_CHANNELS = " https://www.kpn.com/faq/14411"
DIGITENNE_PROBLEMS_OTHER_CARDS = " https://www.kpn.com/faq/8244"
DIGITENNE_PROBLEMS_SMARTCARD = " https://www.kpn.com/faq/10878"

OUTAGE_ALL = "Ik begrijp dat je een storing ervaart, wat vervelend. Ga naar de volgende pagina om stap voor stap je probleem op te lossen: https://www.kpn.com/service/bellen/alledienstenthuis.htm#/problemen-en-storingen/alle-diensten-thuis"
OUTAGE_INTERNET = "Ik begrijp dat je problemen ervaart met je internet, wat vervelend. Ga naar de volgende pagina om stap voor stap je probleem op te lossen: https://www.kpn.com/service/internet/problemen.htm#/problemen-en-storingen/internet"
OUTAGE_LANDLINE= "Ik begrijp dat je problemen ervaart met je vast bellen, wat vervelend. Ga naar de volgende pagina om stap voor stap je probleem op te lossen: https://www.kpn.com/service/bellen/problemen.htm#/problemen-en-storingen/vast-bellen"
OUTAGE_MOBILE = "Ik begrijp dat je een storing ervaart, wat vervelend. Ga naar de volgende pagina om stap voor stap je probleem op te lossen: https://www.kpn.com/service/mobiel/problemen.htm"


#       ANSWERS ERROR CODES
ANSWER_ERRORCODE_12 = " https://www.kpn.com/service/televisie/problemen/foutcode-12-140-141-142-143-144-843.htm#/"
ANSWER_ERRORCODE_300 = " https://www.kpn.com/faq/19546"
ANSWER_ERRORCODE_800 = " https://www.kpn.com/faq/19547"
ANSWER_ERRORCODE_11 = " https://www.kpn.com/service/televisie/problemen/foutcode-11-303-842.htm#/"
ANSWER_ERRORCODE_561 = " https://www.kpn.com/faq/17949"
