from flask import Flask
from flask import request
from handlers.dialogflowRequest import *
from intents import email, outage, compleet
from requests_toolbelt.adapters import appengine
from chatbaseControllerr import chatbase_incoming

appengine.monkeypatch()
app = Flask(__name__)


def default(request):
    """ Returns the default fallback from Dialogflow"""
    return request.create_text_response(request.fulfillment)


""" Action mapping. navigate to the right functions for an action. """
routing = {'input.email.setup': email.email_setup,
           'input.outage': outage.main_outage,
           'input.errorcodes': outage.error_codes,
           'input.compleet': compleet.compleet,
           'input.default' : default,
           'input.stop': default
           }


@app.route('/')
def hello():
    """ Because this is a webhook we don't show a web page. """
    return 'Sorry, nothing at this URL.', 404


@app.route('/', methods=['POST'])
def apiai_response():
    """ Route for incoming dialogflow messages """

    """ Create a request object"""
    response = Request(request.data)
    print(request)

    """ Call the function from the routing dict and pass the Request"""
    my_response = routing[response.action](response)

    """ Send the message in chatbase """
    chatbase_incoming(response)

    return my_response


@app.errorhandler(404)
def page_not_found(e):
    """Return a custom 404 error."""
    return 'Sorry, nothing at this URL.', 404


