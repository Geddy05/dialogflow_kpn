

class Button:
    """ Button object for Dialogflow"""

    def __init__(self,text, postback):
        self.text = text
        self.postback = postback


class Card:
    """ Card object for Dialogflow contains a list of Button objects"""

    def __init__(self, title, subtitle, image_url, button_list):
        self.title = title
        self.subtitle = subtitle
        self.image_uri = image_url
        self.buttons = button_list


class Suggestion:
    """ Suggestion object for Dialogflow"""

    def __init__(self, title):
        self.title = title


class Suggestions:
    """ Suggestions object for Dialogflow contains a list of Suggestion objects"""
    def __init__(self, suggestion_list):
        self.suggestions = suggestion_list
